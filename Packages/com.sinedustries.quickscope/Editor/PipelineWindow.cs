using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;

using UnityEditor;
using UnityEngine;
using UnityEngine.Animations;
using UnityEngine.Playables;

using SINeDUSTRIES.Unity;
using SINeDUSTRIES.QuickScope.Nodes;

namespace SINeDUSTRIES.QuickScope
{
  public class PipelineWindow : EditorWindow
  {
    #region messages

    private void Update()
    {
      if (this.pipeline != null)
      {
        // TODO: wrap Update up into a Pipeline function

        // pipeline, start
        if (this.pipeline.State == RunState.Waiting)
        {
          //Debug.LogFormat("Pipeline: Started");

          this.pipeline.Start();

          return; // start, then delay
        }

        //Debug.LogFormat("Pipeline Running");

        // pipeline, run
        if (this.pipeline.State == RunState.Running)
        {
          if (this.pipeline.Run(out Pipeline_Output output)) // update the job
          {
            // finished
            //Debug.LogFormat("Pipeline Finished");
            this.pipeline = null;
          }
        }
      }
    }

    private void OnGUI()
    {
      EditorGUILayout.LabelField("Config");
      EditorGUI.indentLevel++;
      this.configTarget = (ConfigTarget)EditorGUILayout.ObjectField("Target", this.configTarget, typeof(ConfigTarget), false);
      this.configView = (ConfigView)EditorGUILayout.ObjectField("View", this.configView, typeof(ConfigView), false);
      this.configTextures = (ConfigTextures)EditorGUILayout.ObjectField("Textures", this.configTextures, typeof(ConfigTextures), false);
      this.clips = (AnimationClipArray)EditorGUILayout.ObjectField("Clips", this.clips, typeof(AnimationClipArray), false);
      EditorGUI.indentLevel--;

      // running, display
      if (
        this.pipeline != null
        && this.pipeline.State == RunState.Running
      )
      {
        EditorGUILayout.LabelField($"Running Rotoscope Pipeline...");

        return;
      }

      // not running, start
      if (
        this.configTarget != null
        && GUILayout.Button("Run Rotoscope Job")
      )
      {
        this.pipeline = new Pipeline(this.configTarget, this.configView, this.configTextures, this.clips);

        return;
      }
    }

    #endregion

    #region inspector

    /// <summary>
    /// config for the pipeline.
    /// </summary>
    private ConfigTarget configTarget;

    private ConfigView configView;

    private ConfigTextures configTextures;

    private AnimationClipArray clips;

    #endregion

    #region this / fields

    private Pipeline pipeline;

    #endregion

    #region methods / static

    [MenuItem("Window/QuickScope/Pipeline")]
    public static void ShowWindow()
    {
      EditorWindow.GetWindow(typeof(PipelineWindow));
    }

    #endregion
  }
}