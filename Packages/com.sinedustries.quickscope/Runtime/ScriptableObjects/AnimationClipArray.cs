using System;
using System.Collections.Generic;
using UnityEngine;

namespace SINeDUSTRIES.Unity
{
  // TODO: move to shared library

  /// <summary>
  /// Asset which contains multiple AnimationClip.
  /// </summary>
  [CreateAssetMenu(fileName = nameof(AnimationClipArray), menuName = "Array/" + nameof(AnimationClip))]
  public class AnimationClipArray : ScriptableObject
  {
    #region properties

    /// <summary>
    /// Asset collection.
    /// </summary>
    public IEnumerable<AnimationClip> Assets =>
      this.assets;

    #endregion

    #region fields

    [SerializeField]
    private AnimationClip[] assets;

    #endregion
  }
}