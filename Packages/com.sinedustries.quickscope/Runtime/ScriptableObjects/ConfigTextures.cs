using System;

using UnityEngine;

namespace SINeDUSTRIES.QuickScope
{
  /// <summary>
  /// Describes the textures created during rotoscoping.
  /// </summary>
  [CreateAssetMenu(fileName = nameof(ConfigTextures), menuName = "QuickScope/" + nameof(ConfigTextures))]
  public class ConfigTextures : ScriptableObject,
    IConfigTextures
  {
    #region properties

    /// <summary>
    /// <see cref="IConfigTextures.AntiAliasing"/>.  
    /// </summary>
    public Int32 AntiAliasing =>
      this.antiAliasing;

    #endregion

    #region fields

    [SerializeField]
    private Int32 antiAliasing;

    #endregion
  }
}