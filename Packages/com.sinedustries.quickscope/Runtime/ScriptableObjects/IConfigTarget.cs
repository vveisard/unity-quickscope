using System;

using UnityEngine;

using SINeDUSTRIES.Unity;

namespace SINeDUSTRIES.QuickScope
{
  /// <summary>
  /// Information about
  /// </summary>
  public interface IConfigTarget
  {
    #region config

    /// <summary>
    /// Name of the target being rotoscoped.
    /// </summary>
    String name { get; }

    /// <summary>
    /// Target of the camera.
    /// Searches for <see cref="Animator"/> in children.
    /// </summary>
    GameObject Object { get; }

    /// <summary>
    /// Height of the target.
    /// </summary>
    Single ObjectHeight { get; }

    #endregion
  }
}