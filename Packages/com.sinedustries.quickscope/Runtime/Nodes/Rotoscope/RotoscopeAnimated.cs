using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

using UnityEngine;
using UnityEngine.Animations;
using UnityEngine.Events;
using UnityEngine.Playables;

using UnityEditor;

using SINeDUSTRIES.Unity;

namespace SINeDUSTRIES.QuickScope.Nodes
{
  /// <summary>
  /// Node in the pipeline which does the rotoscoping.
  /// ie, generates <see cref="Sample"/> for graphics, and optionally, depth.
  /// </summary>
  /// <remarks>
  /// Implicitly a Singleton, as it reads from a static class.
  /// Multiple instances have unexpcted behaviours.
  /// </remarks>
  public class RotoscopeAnimated : ARotoscope
  {
    #region function / private / node

    override protected /* concrete */ void start()
    {

    }

    /// <summary>
    /// Continue running this job.
    /// </summary>
    override protected /* concrete */ Boolean run(out Rotoscope_Output output)
    {
      this.rotoscope(this.iClip.name, this.iClip_sampleIndex);
      this.clipEvaluate(); // evaluate clip

      // if the clip is over
      if (
        this.iClip_time > this.iClip.length
        && !Mathf.Approximately(this.iClip_time, this.iClip.length) // additonal check to make sure not at the last frame
      )
      {
        //Debug.LogFormat($"{this.iClip_time} > {this.iClip.length}");

        // if there are no more clips
        if (!this.clipNext())
        {
          output = new Rotoscope_Output(this.outputs);

          return true; // finished running
        }
      }

      output = default(Rotoscope_Output);

      return false;
    }

    override protected /* concrete */ void finish()
    {
      // cleanup playables
      this.playable_Graph.Destroy();

      base.finish();
    }

    #endregion

    #region methods / private / clip

    /// <summary>
    /// Advances time and evaluates clip.
    /// </summary>
    private void clipEvaluate()
    {
      this.iClip_time += this.iClip_delta;
      this.iClip_sampleIndex++;

      this.playable_Graph.Evaluate(this.iClip_delta); // delta afterwards
    }

    /// <summary>
    /// Move to the next clip.
    /// </summary>
    private Boolean clipNext()
    {
      if (this.target_animationClips.MoveNext())
      {
        // increment clip index
        this.iClip_Index++;

        // set values for new clip
        this.iClip_sampleIndex = 0;
        this.iClip_time = 0; // sample time, in seconds
        this.iClip_delta = 1 / this.target_animationClips.Current.frameRate; // number of seconds between each sample. eg, 30fps is one sample every 0.3333 seconds :: 1/ 30

        // add clip to playable
        this.playable_clip = AnimationClipPlayable.Create(this.playable_Graph, this.iClip);
        this.playable_Output.SetSourcePlayable(this.playable_clip);

        // evaluate at start; so that first sample will start at 0
        this.playable_Graph.Evaluate(0);

        return true;
      }
      else
      {
        return false;
      }
    }

    #endregion

    #region this / fields / target

    /// <summary>
    /// AnimationClips
    /// </summary>
    private IEnumerator<AnimationClip> target_animationClips;

    #endregion

    #region this / fields / playable

    private PlayableGraph playable_Graph;

    private AnimationClipPlayable playable_clip;

    private AnimationPlayableOutput playable_Output;

    #endregion

    #region this / fields / clip

    /// <summary>
    /// Current sample count.
    /// </summary>
    private int iClip_sampleIndex;

    /// <summary>
    /// Current sampling time.
    /// </summary>
    private Single iClip_time;

    /// <summary>
    /// Delta between evaluating.
    /// </summary>
    private float iClip_delta;

    /// <summary>
    /// AnimationClip currently being sampled.
    /// </summary>
    private AnimationClip iClip =>
      this.target_animationClips?.Current;

    /// <summary>
    /// Index of the current clip.
    /// </summary>
    private Int32 iClip_Index;

    #endregion

    #region this / fields / entrypoints

    private Int32 target_animationClipCount;

    #endregion

    #region constructors

    public RotoscopeAnimated(IConfigTarget configTarget, IConfigView configView, IConfigTextures configTextures, AnimationClipArray clips) : base(configTarget, configView, configTextures)
    {
      // animations - get animator
      Animator animator = this.target_Clone.GetComponentInChildren<Animator>();
      if (animator == null)
      {
        throw new UnityEngine.MissingComponentException($"Pipeline / Rotoscope: Target {this.configTarget.Object} is missing {nameof(Animator)} Component.");
      }

      // playable
      this.playable_Graph = PlayableGraph.Create();
      this.playable_Graph.SetTimeUpdateMode(DirectorUpdateMode.Manual);
      this.playable_Output = AnimationPlayableOutput.Create(this.playable_Graph, "Animation", animator);

      // clip - get 0th
      this.target_animationClips = clips.Assets.GetEnumerator();
      this.target_animationClipCount = clips.Assets.Count();

      this.iClip_Index = -1; // start at -1, advanced to 0
      this.clipNext();
    }

    #endregion
  }
}