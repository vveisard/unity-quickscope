using System;
using System.Collections.Generic;
using System.IO;

using UnityEngine;

using SINeDUSTRIES.Unity;
using System.Linq;

namespace SINeDUSTRIES.QuickScope.Nodes
{
  /// <summary>
  /// Node in rotoscope pipeline which creates assets from <see cref="Sample"/>
  /// </summary>
  public class Write : ARun</*IWrite_Input,*/ Write_Output>
  {
    // #region this / methods

    // override public void Init(IWrite_Input input)
    // {
    //   base.Init(input);

    //   this.rotoscopeEnumerator = input.Rotoscopes.GetEnumerator();
    //   this.output_paths = new List<String>();
    // }

    // #endregion

    #region ANode / methods

    // override protected /* concrete */ void init(IWrite_Input input)
    // {

    // }

    override protected /* concrete */ void start()
    {

    }

    override protected /* concrete */ void finish()
    {
      // no cleanup required
    }

    override protected /* concrete */ bool run(out Write_Output output)
    {
      if (this.rotoscopeEnumerator.MoveNext())
      {
        Sample current = this.rotoscopeEnumerator.Current;

        // get filename
        String filename = current.Filename();

        // add filename to outputs
        this.output_paths.Add(filename);

        // write sprite
        Texture2DUtils.WriteToPNG(
          current.TextureSprite,
          Path.Combine(Application.dataPath, "QuickScope", "Output", filename + "-sprite.png")
        );

        // write normals
        Texture2DUtils.WriteToPNG(
          current.TextureNormals,
          Path.Combine(Application.dataPath, "QuickScope", "Output", filename + "-normals.png")
        );

        output = default(Write_Output);

        //UnityEngine.Debug.Log($"Pipline / {this.GetType().Name} / run, unfinished");

        return false;
      }
      else
      {
        // no more rotoscopes, job is done

        //UnityEngine.Debug.Log($"Pipline / {this.GetType().Name} / run: finished");

        output = new Write_Output(this.output_paths);
        return true;
      }
    }

    #endregion

    #region this / fields

    private IEnumerator<Sample> rotoscopeEnumerator;

    private IList<String> output_paths;

    #endregion

    #region constructors

    public Write(IEnumerable<Sample> rotoscopes)
    {
      //UnityEngine.Debug.Log($"Pipline / {this.GetType().Name} / ctor: {rotoscopes.Count()}");

      this.rotoscopeEnumerator = rotoscopes.GetEnumerator();
      this.output_paths = new List<String>();
    }

    #endregion
  }
}