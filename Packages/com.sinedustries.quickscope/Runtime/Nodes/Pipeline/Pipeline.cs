using System;
using System.Collections.Generic;

using UnityEngine;

using SINeDUSTRIES.Unity;

namespace SINeDUSTRIES.QuickScope.Nodes
{
  public class Pipeline : ARun<Pipeline_Output>
  {
    #region ARun / methods

    override protected /* concrete */ void start()
    {
      this.nodeRotoscope.Start();
    }

    override protected /* concrete */ Boolean run(out Pipeline_Output output)
    {
      // NodeRotoscope, Run
      if (nodeRotoscope.State == RunState.Running)
      {
        // NodeRotoscope, Finish
        Rotoscope_Output nodeRotoscope_Output;
        if (nodeRotoscope.Run(out nodeRotoscope_Output))
        {
          // NodeWrite, Init
          this.nodeWrite = new Write(nodeRotoscope_Output.Rotoscopes);
        }
      }

      // start write
      if (nodeWrite?.State == RunState.Running)
      {
        if (nodeWrite.Run(out Write_Output outputWrite))
        {
          output = new Pipeline_Output(outputWrite);
          return true;
        }
      }

      // start after run
      // ie, must be started before running
      if (nodeWrite?.State == RunState.Waiting)
      {
        this.nodeWrite.Start();
      }

      // else return false
      output = default(Pipeline_Output);
      return false;
    }

    override protected /* concrete */ void finish()
    {
      //Debug.Log("Pipline: finished");
    }

    #endregion

    #region fields

    private ARotoscope nodeRotoscope;

    private Write nodeWrite;

    #endregion

    #region constructors

    public Pipeline(IConfigTarget configTarget, IConfigView configView, IConfigTextures configTextures, AnimationClipArray clips = null)
    {
      if (clips != null)
      {
        this.nodeRotoscope = new RotoscopeAnimated(configTarget, configView, configTextures, clips);
      }
      else
      {
        this.nodeRotoscope = new RotoscopeStill(configTarget, configView, configTextures);
      }
    }

    #endregion
  }
}